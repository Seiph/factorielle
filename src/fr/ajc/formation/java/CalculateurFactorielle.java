package fr.ajc.formation.java;

public class CalculateurFactorielle {

	public static long factorielle(long entier) {
		if (entier == 0) {
			return 1;
		} else if (entier == 1) {
			return 1;
		} else if (entier > 1) {
			long i = 2;
			long fin = entier;
			long resultat = 1;
			while (i != fin) {
				resultat = resultat * i;
				i = i + 1;
			}
			return resultat * entier;
		} else {
			return 0;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(CalculateurFactorielle.factorielle(0));
		System.out.println(CalculateurFactorielle.factorielle(3));
		System.out.println(CalculateurFactorielle.factorielle(6));
	}
}
